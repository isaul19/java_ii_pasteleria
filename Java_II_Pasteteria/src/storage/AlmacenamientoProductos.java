package storage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import models.Cliente;
import models.Producto;


public class AlmacenamientoProductos {

	private final String fileName = System.getProperty("user.dir") + "/src/storage/almacenamiento-producto.txt";
	private final int startId = 4000;

	public int guardar(Producto producto) {
		try {
			int nuevoId = generarCodigo();
			producto.setCodigo(nuevoId);
			guardar_en__almacenamiento(producto);       
			return nuevoId;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<Producto> listarProducto() {
		try {
			return leer_almacenamiento();
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public List<Producto> buscarProductoPorCodigo(String codigo) {
		List<Producto> productos = listarProducto();

		if (codigo == null || codigo.trim().isEmpty()) {
			return productos;
		}

		List<Producto> coincidencias = new ArrayList<Producto>();
		for (int i = 0; i < productos.size(); i++) {
			Producto producto = productos.get(i);

			if (producto.getCodigo().toString().startsWith(codigo)) {
				coincidencias.add(producto);
			}
		}

		return coincidencias;
	}

	public Producto obtenerProductoPorNombre(String nombre) {
		try {
			List<Producto> productos = leer_almacenamiento();
			for (int i = 0; i < productos.size(); i++) {
				if (productos.get(i).getNombre() == nombre) {
					return productos.get(i);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Producto obtenerProductoPorPrecio(double precio) {
		try {
			List<Producto> productos = leer_almacenamiento();
			for (int i = 0; i < productos.size(); i++) {
				if (productos.get(i).getPrecio() == precio) {
					return productos.get(i);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Boolean eliminarProducto(int codigo) {
		try {
			List<Producto> productos = leer_almacenamiento();
			for (int i = 0; i < productos.size(); i++) {
				if (productos.get(i).getCodigo() == codigo) {
					productos.remove(i);
					reescribir_almacenamiento(productos);
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean actualizarProducto(Producto productoActualizado) {
		try {
			List<Producto> productos = leer_almacenamiento();
			for (int i = 0; i < productos.size(); i++) {
				if (productos.get(i).getCodigo().equals(productoActualizado.getCodigo())) {
					productos.set(i, productoActualizado);
					reescribir_almacenamiento(productos);
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private int generarCodigo() throws IOException {
		List<Producto> productos = leer_almacenamiento();
		int maxId = startId + productos.size();
		return maxId + 1;
	}
	
	public Producto obtenerProductoPorCodigo(int codigo) {
		try {
			List<Producto> productos = leer_almacenamiento();
			for (int i = 0; i < productos.size(); i++) {
				if (productos.get(i).getCodigo() == codigo) {
					return productos.get(i);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<Producto> leer_almacenamiento() throws IOException {
		List<Producto> productos = new ArrayList<>();
		File file = new File(fileName);
		if (!file.exists()) {
			return productos;
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			String linea;
			while ((linea = reader.readLine()) != null) {
				productos.add(Producto.toObject(linea));
			}
		}
		return productos;
	}

	private void guardar_en__almacenamiento(Producto productos) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
			writer.write(productos.toString());
			writer.newLine();
		}
	}

	private void reescribir_almacenamiento(List<Producto> productos) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
			for (int i = 0; i < productos.size(); i++) {
				Producto producto = productos.get(i);
				writer.write(producto.toString());
				writer.newLine();
			}
		}
	}


}
