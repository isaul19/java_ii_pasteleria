package storage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import models.Cliente;
import models.Producto;
import models.Venta;

public class AlmacenamientoVentas {
	private final String fileName = System.getProperty("user.dir") + "/src/storage/almacenamiento-venta.txt";
	private final int startId = 5000;

	public int guardar(Venta venta) {
		try {
			int nuevoId = generarCodigo();
			venta.setCodigo(nuevoId);
			guardar_en_almacenamiento(venta);
			return nuevoId;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<Venta> listarVenta(List<Cliente> clientes, List<Producto> productos) {
		try {
			return leer_almacenamiento(clientes, productos);
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public Venta obtenerVentaPorCodigo(int codigo, List<Cliente> clientes, List<Producto> productos) {
		try {
			List<Venta> Venta = leer_almacenamiento(clientes, productos);
			for (Venta venta : Venta) {
				if (venta.getCodigo().equals(codigo)) {
					return venta;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean eliminarVenta(int codigo, List<Cliente> clientes, List<Producto> productos) {
		try {
			List<Venta> Venta = leer_almacenamiento(clientes, productos);
			for (int i = 0; i < Venta.size(); i++) {
				if (Venta.get(i).getCodigo().equals(codigo)) {
					Venta.remove(i);
					reescribir_almacenamiento(Venta);
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean actualizarVenta(Venta ventaActualizada, List<Cliente> clientes, List<Producto> productos) {
		try {
			List<Venta> Venta = leer_almacenamiento(clientes, productos);
			for (int i = 0; i < Venta.size(); i++) {
				if (Venta.get(i).getCodigo().equals(ventaActualizada.getCodigo())) {
					Venta.set(i, ventaActualizada);
					reescribir_almacenamiento(Venta);
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private int generarCodigo() throws IOException {
		System.out.println(obtenerCantidadTotalDeVentas());
		int maxId = startId + obtenerCantidadTotalDeVentas();
		return maxId + 1;
	}

	public int obtenerCantidadTotalDeVentas() {
		int count = 0;
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			while (reader.readLine() != null) {
				count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}

	private List<Venta> leer_almacenamiento(List<Cliente> clientes, List<Producto> productos) throws IOException {
		List<Venta> ventas = new ArrayList<>();
		File file = new File(fileName);
		if (!file.exists()) {
			return ventas;
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			String linea;
			while ((linea = reader.readLine()) != null) {
				String[] atributos = linea.split(",");
				Cliente cliente = buscarClientePorCodigo(Integer.parseInt(atributos[1]), clientes);
				Producto producto = buscarProductoPorCodigo(Integer.parseInt(atributos[2]), productos);
				if (cliente == null || producto == null)
					continue;

				ventas.add(Venta.toObject(linea, cliente, producto));
			}
		}
		return ventas;
	}

	private void guardar_en_almacenamiento(Venta venta) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
			writer.write(venta.toString());
			writer.newLine();
		}
	}

	private void reescribir_almacenamiento(List<Venta> Venta) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
			for (Venta venta : Venta) {
				writer.write(venta.toString());
				writer.newLine();
			}
		}
	}

	private Cliente buscarClientePorCodigo(int codigo, List<Cliente> clientes) {
		for (Cliente cliente : clientes) {
			if (cliente.getCodigo() == codigo) {
				return cliente;
			}
		}
		return null;
	}

	private Producto buscarProductoPorCodigo(int codigo, List<Producto> productos) {
		for (Producto producto : productos) {
			if (producto.getCodigo() == codigo) {
				return producto;
			}
		}
		return null;
	}
}
