package storage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import models.Cliente;

public class AlmacenamientoClientes {
	private final String fileName = System.getProperty("user.dir") + "/src/storage/almacenamiento-cliente.txt";
	private final int startId = 3000;

	public int guardar(Cliente cliente) {
		try {
			int nuevoId = generarCodigo();
			cliente.setCodigo(nuevoId);
			guardar_en__almacenamiento(cliente);
			return nuevoId;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<Cliente> listarClientes() {
		try {
			return leer_almacenamiento();
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public List<Cliente> buscarClientePorCodigo(String codigo) {
		List<Cliente> clientes = listarClientes();

		if (codigo == null || codigo.trim().isEmpty()) {
			return clientes;
		}

		List<Cliente> coincidencias = new ArrayList<Cliente>();
		for (int i = 0; i < clientes.size(); i++) {
			Cliente cliente = clientes.get(i);

			if (cliente.getCodigo().toString().startsWith(codigo)) {
				coincidencias.add(cliente);
			}
		}

		return coincidencias;
	}

	public Cliente obtenerClientePorDni(int dni) {
		try {
			List<Cliente> clientes = leer_almacenamiento();
			for (int i = 0; i < clientes.size(); i++) {
				if (clientes.get(i).getDni() == dni) {
					return clientes.get(i);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Cliente obtenerClientePorCodigo(int codigo) {
		try {
			List<Cliente> clientes = leer_almacenamiento();
			for (int i = 0; i < clientes.size(); i++) {
				if (clientes.get(i).getCodigo() == codigo) {
					return clientes.get(i);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Boolean eliminarCliente(int codigo) {
		try {
			List<Cliente> clientes = leer_almacenamiento();
			for (int i = 0; i < clientes.size(); i++) {
				if (clientes.get(i).getCodigo() == codigo) {
					clientes.remove(i);
					reescribir_almacenamiento(clientes);
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean actualizarCliente(Cliente clienteActualizado) {
		try {
			List<Cliente> clientes = leer_almacenamiento();
			for (int i = 0; i < clientes.size(); i++) {
				if (clientes.get(i).getCodigo().equals(clienteActualizado.getCodigo())) {
					clientes.set(i, clienteActualizado);
					reescribir_almacenamiento(clientes);
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private int generarCodigo() throws IOException {
		List<Cliente> clientes = leer_almacenamiento();
		int maxId = startId + clientes.size();
		return maxId + 1;
	}

	private List<Cliente> leer_almacenamiento() throws IOException {
		List<Cliente> clientes = new ArrayList<>();
		File file = new File(fileName);
		if (!file.exists()) {
			return clientes;
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			String linea;
			while ((linea = reader.readLine()) != null) {
				clientes.add(Cliente.toObject(linea));
			}
		}
		return clientes;
	}

	private void guardar_en__almacenamiento(Cliente cliente) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
			writer.write(cliente.toString());
			writer.newLine();
		}
	}

	private void reescribir_almacenamiento(List<Cliente> clientes) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
			for (int i = 0; i < clientes.size(); i++) {
				Cliente cliente = clientes.get(i);
				writer.write(cliente.toString());
				writer.newLine();
			}
		}
	}
}
