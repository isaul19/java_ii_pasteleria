package models;

public class Producto {
	private Integer codigo;
	private String nombre;
	private double precio;
	private int stock;
	private int stockMinimo;
	private int stockMaximo;
	
	public Producto(int codigo, String nombre, double precio, int Stock_Actual, int Stock_Minimo, int Stock_Maximo ) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.precio = precio;
		this.stock = Stock_Actual;
		this.stockMinimo = Stock_Minimo;
		this.stockMaximo = Stock_Maximo;
	}

	public Producto(String nombre, double precio, int Stock_Actual, int Stock_Minimo, int Stock_Maximo ) {
		this.codigo = null;
		this.nombre = nombre;
		this.precio = precio;
		this.stock = Stock_Actual;
		this.stockMinimo = Stock_Minimo;
		this.stockMaximo = Stock_Maximo;
	}
	
	@Override
	public String toString() {
		return codigo + "," + nombre + "," + precio + "," + stock + "," + stockMinimo + "," + stockMaximo;
	}

	public static Producto toObject(String productoString) {
		 String[] atributos = productoString.split(",");
		 int codigo = Integer.parseInt(atributos[0]);
		 String nombre = (atributos[1]);
		 double precio = Double.parseDouble(atributos[2]);
		 int Stock_Actual = Integer.parseInt(atributos[3]);
		 int Stock_Minimo = Integer.parseInt(atributos[4]);
		 int Stock_Maximo = Integer.parseInt(atributos[5]);
				 
		 return new Producto(codigo, nombre, precio, Stock_Actual, Stock_Minimo, Stock_Maximo);
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public int getStock() {
		return stock;
	}

	public int getStockMinimo() {
		return stockMinimo;
	}

	public int getStockMaximo() {
		return stockMaximo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public void setStock_Actual(int stock_Actual) {
		stock = stock_Actual;
	}

	public void setStock_Minimo(int stock_Minimo) {
		stockMinimo = stock_Minimo;
	}

	public void setStock_Maximo(int stock_Maximo) {
		stockMaximo = stock_Maximo;
	}
}
