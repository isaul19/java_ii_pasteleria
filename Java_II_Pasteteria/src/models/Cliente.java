package models;

public class Cliente {
	private Integer codigo;
	private int dni;
	private String nombres;
	private String apellidos;
	private int telefono;
	private String direccion;
	
	public Cliente(int codigo, int dni, String nombres, String apellidos, int telefono, String direccion) {
		this.codigo = codigo;
		this.dni = dni;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.direccion = direccion;
	}

	public Cliente(int dni, String nombres, String apellidos, int telefono, String direccion) {
		this.codigo = null;
		this.dni = dni;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return codigo + "," + dni + "," + nombres + "," + apellidos + "," + telefono + "," + direccion;
	}

	public static Cliente toObject(String clienteString) {
		 String[] atributos = clienteString.split(",");
		 int codigo = Integer.parseInt(atributos[0]);
		 int dni = Integer.parseInt(atributos[1]);
		 String nombres = atributos[2];
		 String apellidos = atributos[3];
		 int telefono = Integer.parseInt(atributos[4]);
		 String direccion = atributos[5];
				 
		 return new Cliente(codigo, dni, nombres, apellidos, telefono, direccion);
	}
	
	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
