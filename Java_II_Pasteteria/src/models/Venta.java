package models;

public class Venta {
	private Integer codigo;
	private Cliente cliente;
	private Producto producto;
	private int cantidadUnidades;
	private String fecha;
	private double importeSubtotal;
	private double importeIGV;
	private double importeTotal;

	public Venta(Integer codigoVenta, Cliente cliente, Producto producto, int cantidadUnidades, String fecha) {
		this.codigo = codigoVenta;
		this.cliente = cliente;
		this.producto = producto;
		this.cantidadUnidades = cantidadUnidades;
		this.fecha = fecha;

		this.importeSubtotal = cantidadUnidades * producto.getPrecio();
		this.importeIGV = importeSubtotal * 0.18;
		this.importeTotal = importeSubtotal + importeIGV;
	}

	@Override
	public String toString() {
		return codigo + "," + cliente.getCodigo() + "," + producto.getCodigo() + "," + cantidadUnidades + "," + fecha;
	}

	public static Venta toObject(String ventaString, Cliente cliente, Producto producto) {
		String[] atributos = ventaString.split(",");
		Integer codigo = Integer.parseInt(atributos[0]);
		int cantidadUnidades = Integer.parseInt(atributos[3]);
		String fecha = atributos[4];

		return new Venta(codigo, cliente, producto, cantidadUnidades, fecha);
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigoVenta) {
		this.codigo = codigoVenta;
	}

	public Integer getCodigoCliente() {
		return cliente.getCodigo();
	}

	public void setCodigoCliente(Integer codigoCliente) {
		this.cliente.setCodigo(codigoCliente);
	}

	public Integer getCodigoProducto() {
		return producto.getCodigo();
	}

	public void setCodigoProducto(int codigoProducto) {
		this.producto.setCodigo(codigoProducto);
	}

	public Integer getcantidadUnidades() {
		return cantidadUnidades;
	}

	public void setcantidadUnidades(Integer cantidadUnidades) {
		this.cantidadUnidades = cantidadUnidades;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getImporteSubtotal() {
		return importeSubtotal;
	}

	public void setImporteSubtotal(double importeSubtotal) {
		this.importeSubtotal = importeSubtotal;
	}

	public double getImporteIGV() {
		return importeIGV;
	}

	public void setImporteIGV(double importeIGV) {
		this.importeIGV = importeIGV;
	}

	public double getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(double importeTotal) {
		this.importeTotal = importeTotal;
	}
}
