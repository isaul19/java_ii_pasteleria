package utils;
import java.awt.Image;
import javax.swing.ImageIcon;

public class CustomImage {
	public static ImageIcon create(String filename) {
		String basePath = System.getProperty("user.dir") + "/assets/images/";
		ImageIcon imgicon = new ImageIcon(basePath + filename);
		Image img = imgicon.getImage();
		Image newImg = img.getScaledInstance(10, 10, Image.SCALE_SMOOTH);
		return new ImageIcon(newImg);
	}
}