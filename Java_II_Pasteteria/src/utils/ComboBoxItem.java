package utils;

public class ComboBoxItem {
	private String label;
	private int value;

	public ComboBoxItem(String label, int value) {
		this.label = label;
		this.value = value;
	}

	@Override
	public String toString() {
		return label;
	}

	public ComboBoxItem toObject() {
		return this;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
