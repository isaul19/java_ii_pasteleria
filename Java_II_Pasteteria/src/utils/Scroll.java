package utils;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class Scroll {
	public static void scrollToTop(JScrollPane scrollComponent) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				scrollComponent.getVerticalScrollBar().setValue(0);
			}
		});
	}
}