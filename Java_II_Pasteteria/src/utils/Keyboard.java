package utils;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/*
======================================================================
  EJEMPLOS DE USO:

  JTextField txtNumeroDeBotellas = new JTextField();
  Keyboard.allowInt(txtNumeroDeBotellas);

  ---

  JTextField txtPrecio = new JTextField();
  Keyboard.allowNumber(txtPrecio);
*/

public class Keyboard {

	public static void allowInt(JTextField textField) {
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char key = e.getKeyChar();
				if (key < '0' || key > '9') {
					e.consume();
				}
			}
		});
	}

	public static void allowNumber(JTextField textField) {
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char key = e.getKeyChar();
				String text = textField.getText();
				Boolean existDot = text.indexOf(".") != -1;

				if (text.trim().length() == 0 && key == '.')
					e.consume();

				if (!existDot && key == '.')
					return;

				if (key < '0' || key > '9') {
					e.consume();
				}
			}
		});
	}
}