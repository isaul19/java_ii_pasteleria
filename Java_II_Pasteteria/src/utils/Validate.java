package utils;

import javax.swing.JOptionPane;

public class Validate {
	public static boolean isEmpty(String data, String fieldName) {
		if (data == null || data.trim().isEmpty()) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' es requerido");
			return true;
		}
		return false;
	}

	public static boolean isNumeric(String data, String fieldName) {
		try {
			Double.parseDouble(data);
			return true;
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser un número válido");
			return false;
		}
	}

	public static boolean isPositiveNumeric(String data, String fieldName) {
		if (!isNumeric(data, fieldName)) {
			return false;
		}

		double number = Double.parseDouble(data);
		if (number <= 0) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser un número positivo");
			return false;
		}
		return true;
	}

	public static boolean isInteger(String data, String filedName) {
		try {
			Integer.parseInt(data);
			return true;
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "El campo '" + filedName + "' debe ser un número entero válido");
			return false;
		}
	}

	public static boolean isPositiveInteger(String data, String fieldName) {
		if (!isInteger(data, fieldName)) {
			return false;
		}
		int number = Integer.parseInt(data);
		if (number <= 0) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser un número entero positivo");
			return false;
		}
		return true;
	}

	public static boolean hasMinLength(String data, int minLength, String fieldName) {
		if (data == null || data.length() < minLength) {
			JOptionPane.showMessageDialog(null,
					"El campo '" + fieldName + "' debe tener al menos " + minLength + " caracteres");
			return false;
		}
		return true;
	}

	public static boolean hasMaxLength(String data, int maxLength, String fieldName) {
		if (data == null || data.length() > maxLength) {
			JOptionPane.showMessageDialog(null,
					"El campo '" + fieldName + "' debe tener como máximo " + maxLength + " caracteres");
			return false;
		}
		return true;
	}

	public static boolean hasExactLength(String data, int length, String fieldName) {
		if (data == null || data.length() != length) {
			JOptionPane.showMessageDialog(null,
					"El campo '" + fieldName + "' debe tener exactamente " + length + " caracteres");
			return false;
		}
		return true;
	}

	public static boolean isGreaterThan(String data, double value, String fieldName) {
		if (!isNumeric(data, fieldName)) {
			return false;
		}
		double number = Double.parseDouble(data);
		if (number <= value) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser mayor que " + value);
			return false;
		}
		return true;
	}

	public static boolean isLessThan(String data, double value, String fieldName) {
		if (!isNumeric(data, fieldName)) {
			return false;
		}
		double number = Double.parseDouble(data);
		if (number >= value) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser menor que " + value);
			return false;
		}
		return true;
	}

	public static boolean isGreaterThanOrEqual(String data, double value, String fieldName) {
		if (!isNumeric(data, fieldName)) {
			return false;
		}
		double number = Double.parseDouble(data);
		if (number < value) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser mayor o igual que " + value);
			return false;
		}
		return true;
	}

	public static boolean isLessThanOrEqual(String data, double value, String fieldName) {
		if (!isNumeric(data, fieldName)) {
			return false;
		}
		double number = Double.parseDouble(data);
		if (number > value) {
			JOptionPane.showMessageDialog(null, "El campo '" + fieldName + "' debe ser menor o igual que " + value);
			return false;
		}
		return true;
	}

}