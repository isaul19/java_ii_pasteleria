package gui.ventas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import models.Cliente;
import models.Producto;
import models.Venta;
import storage.AlmacenamientoClientes;
import storage.AlmacenamientoProductos;
import storage.AlmacenamientoVentas;
import utils.ComboBoxItem;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class RealizarVenta extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtCantidadUnidades;
	private JTextField txtPrecioUnitario;

	private JComboBox<ComboBoxItem> comboBoxCliente;
	private JComboBox<ComboBoxItem> comboBoxProducto;

	private AlmacenamientoVentas almacenamientoVentas = new AlmacenamientoVentas();
	private AlmacenamientoClientes almacenamientoClientes = new AlmacenamientoClientes();
	private AlmacenamientoProductos almacenamientoProductos = new AlmacenamientoProductos();

	JTextArea txtRespuesta;
	private JButton btnVender;
	private JButton btnCancel;
	private JLabel lblTitle;

	public RealizarVenta() {
		setBounds(100, 100, 506, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JLabel lblNombreCliente = new JLabel("Nombre del cliente:");
		lblNombreCliente.setBounds(31, 68, 150, 14);
		contentPanel.add(lblNombreCliente);

		comboBoxCliente = new JComboBox<>();
		comboBoxCliente.setBounds(191, 65, 200, 20);
		contentPanel.add(comboBoxCliente);

		JLabel lblNombreProducto = new JLabel("Nombre del producto:");
		lblNombreProducto.setBounds(31, 99, 150, 14);
		contentPanel.add(lblNombreProducto);

		comboBoxProducto = new JComboBox<>();
		comboBoxProducto.setBounds(191, 96, 200, 20);
		contentPanel.add(comboBoxProducto);

		JLabel lblCantidadUnidades = new JLabel("Cantidad del producto:");
		lblCantidadUnidades.setBounds(31, 130, 150, 14);
		contentPanel.add(lblCantidadUnidades);

		txtCantidadUnidades = new JTextField();
		txtCantidadUnidades.setBounds(191, 127, 200, 20);
		contentPanel.add(txtCantidadUnidades);
		txtCantidadUnidades.setColumns(10);

		JLabel lblPrecioUnitario = new JLabel("Precio unitario:");
		lblPrecioUnitario.setBounds(31, 161, 150, 14);
		contentPanel.add(lblPrecioUnitario);

		txtPrecioUnitario = new JTextField();
		txtPrecioUnitario.setBounds(191, 158, 200, 20);
		txtPrecioUnitario.setEditable(false);
		contentPanel.add(txtPrecioUnitario);
		txtPrecioUnitario.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 220, 408, 185);
		contentPanel.add(scrollPane);

		txtRespuesta = new JTextArea();
		txtRespuesta.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 13));
		scrollPane.setViewportView(txtRespuesta);

		lblTitle = new JLabel("Realizar Venta");
		lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 21));
		lblTitle.setBounds(163, 11, 187, 33);
		contentPanel.add(lblTitle);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		btnVender = new JButton("Vender");
		buttonPane.add(btnVender);

		btnCancel = new JButton("Cancelar");
		buttonPane.add(btnCancel);

		cargarClientes();
		cargarProductos();

		comboBoxCliente.addActionListener(this);
		comboBoxProducto.addActionListener(this);
		btnVender.addActionListener(this);
		btnCancel.addActionListener(this);
	}

	private void cargarClientes() {
		ArrayList<ComboBoxItem> clientes = new ArrayList<>();
		almacenamientoClientes.listarClientes().forEach(cliente -> clientes
				.add(new ComboBoxItem(cliente.getNombres() + " " + cliente.getApellidos(), cliente.getCodigo())));
		comboBoxCliente.setModel(new javax.swing.DefaultComboBoxModel<>(clientes.toArray(new ComboBoxItem[0])));
	}

	private void cargarProductos() {
		ArrayList<ComboBoxItem> productos = new ArrayList<>();
		almacenamientoProductos.listarProducto()
				.forEach(producto -> productos.add(new ComboBoxItem(producto.getNombre(), producto.getCodigo())));

		ComboBoxItem primerProductoItem = productos.get(0);
		Producto primerProducto = almacenamientoProductos.obtenerProductoPorCodigo(primerProductoItem.getValue());
		txtPrecioUnitario.setText(String.valueOf(primerProducto.getPrecio()));
		comboBoxProducto.setModel(new javax.swing.DefaultComboBoxModel<>(productos.toArray(new ComboBoxItem[0])));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == comboBoxProducto) {
			cambiarProducto();
		}

		if (e.getSource() == btnVender) {
			guardarVenta();
		}
		if (e.getSource() == btnCancel) {
			dispose();
		}
	}

	private void cambiarProducto() {
		ComboBoxItem selectedItem = (ComboBoxItem) comboBoxProducto.getSelectedItem();
		if (selectedItem != null) {
			Producto producto = almacenamientoProductos.obtenerProductoPorCodigo(selectedItem.getValue());
			txtPrecioUnitario.setText(String.valueOf(producto.getPrecio()));
		}
	}

	private void guardarVenta() {
		try {
			ComboBoxItem clienteItem = (ComboBoxItem) comboBoxCliente.getSelectedItem();
			ComboBoxItem productoItem = (ComboBoxItem) comboBoxProducto.getSelectedItem();
			int cantidadUnidades = Integer.parseInt(txtCantidadUnidades.getText());

			Cliente cliente = almacenamientoClientes.obtenerClientePorCodigo(clienteItem.getValue());
			Producto producto = almacenamientoProductos.obtenerProductoPorCodigo(productoItem.getValue());

			if (!validarStock(producto, cantidadUnidades)) {
				JOptionPane.showMessageDialog(null, "Stock insuficiente para realizar la venta.");
				return;
			}

			Date fechaActual = new Date();
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
			String fechaFormateada = formato.format(fechaActual);
			Venta venta = new Venta(null, cliente, producto, cantidadUnidades, fechaFormateada);
			int guardarExitoso = almacenamientoVentas.guardar(venta);

			if (guardarExitoso == -1) {
				JOptionPane.showMessageDialog(null, "Error al guardar la venta.");
				return;
			}

			actualizarBoleta(venta);
			producto.setStock_Actual(producto.getStock() - cantidadUnidades);
			almacenamientoProductos.actualizarProducto(producto);
			JOptionPane.showMessageDialog(null, "Venta guardada con éxito");
		} catch (NumberFormatException e) {
			System.out.println("Por favor, ingrese valores válidos.");
		}
	}

	private boolean validarStock(Producto producto, int cantidadUnidades) {
		return (producto.getStock() - cantidadUnidades) >= producto.getStockMinimo();
	}

	private void actualizarBoleta(Venta venta) {
		txtRespuesta.setText("");

		Cliente cliente = almacenamientoClientes.obtenerClientePorCodigo(venta.getCodigoCliente());
		Producto producto = almacenamientoProductos.obtenerProductoPorCodigo(venta.getCodigoProducto());

		StringBuilder boleta = new StringBuilder();
		boleta.append("Código del cliente    : " + cliente.getCodigo() + " " + cliente.getApellidos() + "\n");
		boleta.append("Nombres del cliente   : " + cliente.getNombres() + "\n");
		boleta.append("Código del producto   : " + producto.getCodigo() + "\n");
		boleta.append("Nombre del producto   : " + producto.getNombre() + "\n");
		boleta.append("Cantidad comprada     : " + venta.getcantidadUnidades() + "\n");
		boleta.append("Precio unitario       : " + producto.getPrecio() + "\n");
		boleta.append("Importe subtotal      : " + venta.getImporteSubtotal() + "\n");
		boleta.append("Importe del IGV       : " + venta.getImporteIGV() + "\n");
		boleta.append("Importe total a pagar : " + venta.getImporteTotal() + "\n");

		txtRespuesta.setText(boleta.toString());
	}

}
