package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.almacen.AgregarStock;
import gui.almacen.MantenimientoStockGui;
import gui.matenimiento.clientes.MantenimientoClientesGui;
import gui.matenimiento.productos.MantenimientoProductos;
import gui.reportes.productos.ListadoProductos;
import gui.reportes.ventas.ListadoGeneralVentas;
import gui.ventas.RealizarVenta;
import models.Venta;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Principal extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JMenuItem mntmSalir;
	JMenuItem mntmCliente;
	JMenuItem mntmProductos;
	JMenuItem mntmVentas;
	JMenuItem mntmProducto;
	JMenuItem mntmRealizarVenta;
	JMenuItem mntmAgregarStock;

	public Principal() {
		cargarInterfaz();
	}

	private void cargarInterfaz() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JMenuBar menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);

		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);

		mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(this);
		mnArchivo.add(mntmSalir);

		JMenu mnMantenimiento = new JMenu("Mantenimiento");
		menuBar.add(mnMantenimiento);

		mntmCliente = new JMenuItem("Clientes");
		mntmCliente.addActionListener(this);
		mnMantenimiento.add(mntmCliente);

		mntmProductos = new JMenuItem("Productos");
		mntmProductos.addActionListener(this);
		mnMantenimiento.add(mntmProductos);

		JMenu mnVentas = new JMenu("Ventas");
		menuBar.add(mnVentas);

		JMenu mnAlmacen = new JMenu("Almacén");
		menuBar.add(mnAlmacen);

		JMenu mnReportes = new JMenu("Reportes");
		menuBar.add(mnReportes);

		mntmVentas = new JMenuItem("Listado General");
		mntmVentas.addActionListener(this);
		mnReportes.add(mntmVentas);

		mntmProducto = new JMenuItem("Listado de Productos");
		mntmProducto.addActionListener(this);
		mnReportes.add(mntmProducto);

		mntmRealizarVenta = new JMenuItem("Realizar Venta");
		mntmRealizarVenta.addActionListener(this);
		mnVentas.add(mntmRealizarVenta);

		mntmAgregarStock = new JMenuItem("Agregar stock");
		mntmAgregarStock.addActionListener(this);
		mnAlmacen.add(mntmAgregarStock);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mntmSalir) {
			System.exit(0);
		}

		if (e.getSource() == mntmCliente) {
			new MantenimientoClientesGui().setVisible(true);
		}

		if (e.getSource() == mntmProductos) {
			new MantenimientoProductos().setVisible(true);
		}

		if (e.getSource() == mntmVentas) {
			List<Venta> ventas = ListadoGeneralVentas.obtenerVentasDesdeAlgunaFuente();
			new ListadoGeneralVentas(ventas).setVisible(true);
		}

		if (e.getSource() == mntmProducto) {
			new ListadoProductos().setVisible(true);
		}

		if (e.getSource() == mntmRealizarVenta) {
			new RealizarVenta().setVisible(true);
		}

		if (e.getSource() == mntmAgregarStock) {
			new MantenimientoStockGui().setVisible(true);
		}
	}
}
