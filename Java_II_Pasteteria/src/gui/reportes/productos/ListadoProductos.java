package gui.reportes.productos;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import gui.reportes.ventas.ListadoGeneralVentas;
import models.Producto;
import models.Venta;

public class ListadoProductos extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public ListadoProductos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel panelStockMinimo = new JPanel();
		tabbedPane.addTab("Stock Bajo Mínimo", null, panelStockMinimo, null);
		panelStockMinimo.setLayout(new BorderLayout(0, 0));
		JTextArea textAreaStockMinimo = new JTextArea();
		textAreaStockMinimo.setEditable(false);
		JScrollPane scrollPaneStockMinimo = new JScrollPane(textAreaStockMinimo);
		panelStockMinimo.add(scrollPaneStockMinimo, BorderLayout.CENTER);

		JPanel panelUnidadesVendidas = new JPanel();
		tabbedPane.addTab("Unidades Vendidas", null, panelUnidadesVendidas, null);
		panelUnidadesVendidas.setLayout(new BorderLayout(0, 0));
		JTextArea textAreaUnidadesVendidas = new JTextArea();
		textAreaUnidadesVendidas.setEditable(false);
		JScrollPane scrollPaneUnidadesVendidas = new JScrollPane(textAreaUnidadesVendidas);
		panelUnidadesVendidas.add(scrollPaneUnidadesVendidas, BorderLayout.CENTER);

		JPanel panelImporteTotalAcumulado = new JPanel();
		tabbedPane.addTab("Importe Total Acumulado", null, panelImporteTotalAcumulado, null);
		panelImporteTotalAcumulado.setLayout(new BorderLayout(0, 0));
		JTextArea textAreaImporteTotal = new JTextArea();
		textAreaImporteTotal.setEditable(false);
		JScrollPane scrollPaneImporteTotal = new JScrollPane(textAreaImporteTotal);
		panelImporteTotalAcumulado.add(scrollPaneImporteTotal, BorderLayout.CENTER);

		List<Producto> productos = new ArrayList<>();
		productos.add(new Producto(201, "Maleta", 100.0, 20, 10, 49));
		productos.add(new Producto(202, "Bolso", 150.0, 20, 10, 50));
		List<Venta> ventas = ListadoGeneralVentas.obtenerVentasDesdeAlgunaFuente();

		mostrarProductosConStockBajoMinimo(productos, textAreaStockMinimo);

		mostrarProductosPorUnidadesVendidas(ventas, textAreaUnidadesVendidas);

		mostrarProductosPorImporteTotal(ventas, textAreaImporteTotal);
	}

	private void mostrarProductosConStockBajoMinimo(List<Producto> productos, JTextArea textArea) {
		for (Producto producto : productos) {
			if (producto.getStock() < producto.getStockMinimo()) {
				textArea.append("Código del Producto: " + producto.getCodigo() + "\n");
				textArea.append("Nombre del Producto: " + producto.getNombre() + "\n");
				textArea.append("Stock Actual: " + producto.getStock() + "\n");
				textArea.append("Stock Mínimo: " + producto.getStockMinimo() + "\n");
				textArea.append("---------------------------------------\n");
			}
		}
	}

	private void mostrarProductosPorUnidadesVendidas(List<Venta> ventas, JTextArea textArea) {
		System.out.println(ventas.size());
		for (Venta venta : ventas) {
			System.out.println(venta.getCodigoProducto());
			int codigoProducto = venta.getCodigoProducto();
			int unidadesVendidas = venta.getcantidadUnidades();

			textArea.append("Código del Producto: " + codigoProducto + "\n");
			textArea.append("Unidades Vendidas Acumuladas: " + unidadesVendidas + "\n");
			textArea.append("---------------------------------------\n");
		}
	}

	private void mostrarProductosPorImporteTotal(List<Venta> ventas, JTextArea textArea) {
		Map<Integer, Double> importeTotalPorProducto = new HashMap<>();

		for (Venta venta : ventas) {
			int codigoProducto = venta.getCodigoProducto();
			double importeVenta = venta.getImporteTotal();

			if (importeTotalPorProducto.containsKey(codigoProducto)) {
				double importeActual = importeTotalPorProducto.get(codigoProducto);
				importeTotalPorProducto.put(codigoProducto, importeActual + importeVenta);
			} else {
				importeTotalPorProducto.put(codigoProducto, importeVenta);
			}
		}

		for (Map.Entry<Integer, Double> entry : importeTotalPorProducto.entrySet()) {
			int codigoProducto = entry.getKey();
			double importeTotalAcumulado = entry.getValue();

			textArea.append("Código del Producto: " + codigoProducto + "\n");
			textArea.append("Importe Total Acumulado: " + importeTotalAcumulado + "\n");
			textArea.append("---------------------------------------\n");
		}
	}

}
