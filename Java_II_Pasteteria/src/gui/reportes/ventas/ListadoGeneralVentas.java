package gui.reportes.ventas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import models.Cliente;
import models.Producto;
import models.Venta;

public class ListadoGeneralVentas extends JFrame {

	private JTextArea textArea;

	public ListadoGeneralVentas(List<Venta> ventas) {
		setTitle("Listado general de ventas");
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		textArea = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(textArea);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		StringBuilder sb = new StringBuilder();
		sb.append("Listado general de ventas:\n");
		sb.append("---------------------------------------\n");
		for (Venta venta : ventas) {
			sb.append("Código de venta: ").append(venta.getCodigo()).append("\n");
			sb.append("Código de cliente: ").append(venta.getCodigoCliente()).append("\n");
			sb.append("Código de producto: ").append(venta.getCodigoProducto()).append("\n");
			sb.append("Fecha: ").append(venta.getFecha()).append("\n");
			sb.append("Importe subtotal: ").append(venta.getImporteSubtotal()).append("\n");
			sb.append("Importe del IGV: ").append(venta.getImporteIGV()).append("\n");
			sb.append("Importe total: ").append(venta.getImporteTotal()).append("\n");
			sb.append("---------------------------------------\n");
		}

		textArea.setText(sb.toString());

		setVisible(true);
	}

	public static List<Venta> obtenerVentasDesdeAlgunaFuente() {
		List<Venta> ventas = new ArrayList<>();

		Cliente cliente1 = new Cliente(101, 12345678, "Juan", "Perez", 987654321, "Calle Falsa 123");
		Cliente cliente2 = new Cliente(102, 87654321, "Maria", "Gomez", 123456789, "Avenida Siempreviva 742");
		Producto producto1 = new Producto(124, "Maleta", 100.0, 5, 10, 23);
		Producto producto2 = new Producto(1243, "Bolso", 150.0, 5, 10, 23);

		ventas.add(new Venta(1, cliente1, producto1, 1, "01/01/2023"));
		ventas.add(new Venta(2, cliente2, producto2, 1, "02/01/2023"));
		ventas.add(new Venta(3, cliente1, producto2, 2, "03/01/2023"));

		return ventas;
	}
}
