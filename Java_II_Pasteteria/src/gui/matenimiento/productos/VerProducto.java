package gui.matenimiento.productos;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.matenimiento.productos.MantenimientoProductos.ProductoHandler;
import models.Cliente;
import models.Producto;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class VerProducto extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtStockActual;
	private JTextField txtStockMinimo;
	private JTextField txtStockMaximo;

	private JButton btnCerrar;

	public VerProducto(ProductoHandler productoHandler) {

		cargarInterfaz();
		Producto cliente = productoHandler.obtenerProductoSeleccionado();
		mostrarProducto(cliente);
	}

	private void cargarInterfaz() {
		setBounds(100, 100, 443, 381);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 11, 134, 22);
		contentPanel.add(lblNombre);

		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(10, 57, 134, 22);
		contentPanel.add(lblPrecio);

		JLabel lblStockActual = new JLabel("Stock Actual");
		lblStockActual.setBounds(10, 108, 134, 22);
		contentPanel.add(lblStockActual);

		JLabel lblStockMinimo = new JLabel("Stock Minimo");
		lblStockMinimo.setBounds(10, 161, 134, 22);
		contentPanel.add(lblStockMinimo);

		JLabel lblStockMaximo = new JLabel("Stock Maximo");
		lblStockMaximo.setBounds(10, 219, 134, 22);
		contentPanel.add(lblStockMaximo);

		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setBounds(156, 12, 261, 21);
		contentPanel.add(txtNombre);
		txtNombre.setColumns(10);

		txtPrecio = new JTextField();
		txtPrecio.setEditable(false);
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(154, 58, 261, 21);
		contentPanel.add(txtPrecio);

		txtStockActual = new JTextField();
		txtStockActual.setEditable(false);
		txtStockActual.setColumns(10);
		txtStockActual.setBounds(156, 109, 261, 21);
		contentPanel.add(txtStockActual);

		txtStockMinimo = new JTextField();
		txtStockMinimo.setEditable(false);
		txtStockMinimo.setColumns(10);
		txtStockMinimo.setBounds(154, 162, 261, 21);
		contentPanel.add(txtStockMinimo);

		txtStockMaximo = new JTextField();
		txtStockMaximo.setEditable(false);
		txtStockMaximo.setColumns(10);
		txtStockMaximo.setBounds(154, 220, 261, 21);
		contentPanel.add(txtStockMaximo);

		btnCerrar = new JButton("Cerrar");
		btnCerrar.setBounds(156, 276, 89, 23);
		btnCerrar.addActionListener(this);
		contentPanel.add(btnCerrar);

	}

	private void mostrarProducto(Producto producto) {
		if (producto != null) {
			// txtCodigo.setText(producto.getCodigo().toString());
			txtNombre.setText(String.valueOf(producto.getNombre()));
			txtPrecio.setText(Double.toString(producto.getPrecio()));
			txtStockActual.setText(Integer.toString(producto.getStock()));
			txtStockMinimo.setText(Integer.toString(producto.getStockMinimo()));
			txtStockMaximo.setText(Integer.toString(producto.getStockMaximo()));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnCerrar) {
			dispose();
		}
	}
}
