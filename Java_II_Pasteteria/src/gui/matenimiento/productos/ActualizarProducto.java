package gui.matenimiento.productos;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.matenimiento.productos.MantenimientoProductos.ProductoHandler;
import models.Producto;
import utils.Validate;

public class ActualizarProducto extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private Producto producto;
	private ProductoHandler productoHandler;

	private JTextField txtNombre;
	private JTextField txtPrecio;

	private JButton btnActualizar;
	private JButton btnCancelar;

	public ActualizarProducto(ProductoHandler productoHandler) {

		this.productoHandler = productoHandler;
		this.producto = productoHandler.obtenerProductoSeleccionado();

		cargarInterfaz();
		cargarDatosProducto();
	}

	private void cargarInterfaz() {
		setTitle("Actualizar Producto");
		setBounds(100, 100, 437, 399);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(70, 117, 101, 25);
		contentPanel.add(lblNombre);

		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(76, 169, 89, 25);
		contentPanel.add(lblPrecio);

		txtNombre = new JTextField();
		txtNombre.setBounds(175, 118, 217, 23);
		contentPanel.add(txtNombre);
		txtNombre.setColumns(10);

		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(175, 170, 217, 23);
		contentPanel.add(txtPrecio);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(82, 250, 89, 23);
		btnCancelar.addActionListener(this);
		contentPanel.add(btnCancelar);

		btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(this);
		btnActualizar.setBounds(244, 250, 89, 23);
		contentPanel.add(btnActualizar);

		JLabel lblTitle = new JLabel("Actualizar Producto");
		lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 21));
		lblTitle.setBounds(121, 23, 205, 38);
		contentPanel.add(lblTitle);
	}

	private void cargarDatosProducto() {
		if (producto != null) {

			txtNombre.setText(String.valueOf(producto.getNombre()));
			txtPrecio.setText(String.valueOf(producto.getPrecio()));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnActualizar) {
			actualizarProducto();
		}

		if (e.getSource() == btnCancelar) {
			dispose();
		}
	}

	private void actualizarProducto() {
		if (!formularioRellenadoCorrectamente()) {
			return;
		}

		String nuevoNombre = txtNombre.getText().trim();
		double nuevoPrecio = Double.parseDouble(txtPrecio.getText().trim());

		producto.setNombre(nuevoNombre);
		producto.setPrecio(nuevoPrecio);

		productoHandler.actualizarProducto(producto);
		dispose();
	}

	private boolean formularioRellenadoCorrectamente() {
		if (Validate.isEmpty(txtNombre.getText(), "Nombre")) {
			return false;
		}

		if (Validate.isPositiveNumeric(txtPrecio.getText(), "Precio")) {
			return false;
		}

		return true;
	}
}
