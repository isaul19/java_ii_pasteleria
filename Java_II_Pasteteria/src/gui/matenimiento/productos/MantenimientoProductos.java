package gui.matenimiento.productos;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;

import models.Producto;
import storage.AlmacenamientoProductos;

public class MantenimientoProductos extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private AlmacenamientoProductos almacenamiento = new AlmacenamientoProductos();
	ProductoHandler productoHandler = new ProductoHandler();

	private JTable table;
	private JTextField txtCodigoProducto;
	private JButton btnVer;
	private JButton btnEditar;
	private JButton btnEliminar;
	private JButton btnCrear;

	public MantenimientoProductos() {
		setBounds(100, 100, 577, 599);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 260, 541, 241);
		contentPanel.add(scrollPane);

		table = new JTable();
		DefaultTableModel model = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		model.addColumn("Código de producto");
		model.addColumn("Nombre");
		model.addColumn("Precio");
		table.setModel(model);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);

		inicializarDatosDeLaTabla();

		JLabel lblTitulo = new JLabel("Gestión de Productos");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblTitulo.setBounds(10, 11, 292, 38);
		contentPanel.add(lblTitulo);

		JLabel lblSubtitulo1 = new JLabel("Crear Producto");
		lblSubtitulo1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSubtitulo1.setBounds(10, 60, 178, 30);
		contentPanel.add(lblSubtitulo1);

		JLabel lblDescripcion1 = new JLabel(
				"<html> Has clic en el botón para abrir una pestaña <br> donde podrás ingresar información de un nuevo producto </html>");
		lblDescripcion1.setBounds(10, 85, 361, 42);
		contentPanel.add(lblDescripcion1);

		JLabel lblSubtitulo2 = new JLabel("Buscar Producto");
		lblSubtitulo2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSubtitulo2.setBounds(10, 138, 178, 30);
		contentPanel.add(lblSubtitulo2);

		JLabel lblDescripcion2 = new JLabel(
				"<html>Ingresa el codigo del producto en el campo de búsqueda <br> para encontar detalles del producto <html>");
		lblDescripcion2.setBounds(10, 163, 361, 46);
		contentPanel.add(lblDescripcion2);

		btnCrear = new JButton("Crear");
		btnCrear.setBounds(378, 81, 89, 23);
		btnCrear.addActionListener(this);
		contentPanel.add(btnCrear);

		txtCodigoProducto = new JTextField();
		txtCodigoProducto.setBounds(10, 220, 270, 25);
		contentPanel.add(txtCodigoProducto);
		txtCodigoProducto.setColumns(10);

		escucharCambiosDeCodigoProducto();

		btnVer = new JButton("Ver");
		btnVer.setBounds(27, 526, 89, 23);
		btnVer.addActionListener(this);
		contentPanel.add(btnVer);

		btnEditar = new JButton("Editar");
		btnEditar.setBounds(237, 526, 89, 23);
		btnEditar.addActionListener(this);
		contentPanel.add(btnEditar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(427, 526, 89, 23);
		btnEliminar.addActionListener(this);
		contentPanel.add(btnEliminar);
	}

	private void escucharCambiosDeCodigoProducto() {
		txtCodigoProducto.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				productoHandler.buscarProducto();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				productoHandler.buscarProducto();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				productoHandler.buscarProducto();
			}
		});

	}

	private void rellenarTabla(List<Producto> products) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);

		for (int i = 0; i < products.size(); i++) {
			Producto prodcuto = products.get(i);
			Object[] row = { prodcuto.getCodigo(), prodcuto.getNombre(), prodcuto.getPrecio() };
			model.addRow(row);
		}
	}

	private void inicializarDatosDeLaTabla() {
		List<Producto> productos = almacenamiento.listarProducto();
		rellenarTabla(productos);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnCrear) {
			new CrearProducto(productoHandler).setVisible(true);
			;
			return;

		}

		if (productoHandler.obtenerProductoSeleccionado() == null) {
			return;
		}

		if (e.getSource() == btnVer) {
			new VerProducto(productoHandler).setVisible(true);

		}

		if (e.getSource() == btnEditar) {
			new ActualizarProducto(productoHandler).setVisible(true);

		}

		if (e.getSource() == btnEliminar) {
			productoHandler.eliminarProductoSeleccionado();

		}

	}

	public class ProductoHandler {
		public Producto obtenerProductoSeleccionado() {
			int selectedRow = table.getSelectedRow();
			if (selectedRow != -1) {
				int productoCodigo = (int) table.getValueAt(selectedRow, 0);
				Producto producto = almacenamiento.obtenerProductoPorCodigo(productoCodigo);
				return producto;
			}

			JOptionPane.showMessageDialog(null, "Primero selecciona una fila");
			return null;
		}

		public void crearProducto(Producto producto) {
			int productoCodigo = almacenamiento.guardar(producto);
			if (productoCodigo == -1) {
				JOptionPane.showMessageDialog(null, "Hubo un error al crear el producto");
				return;
			}

			JOptionPane.showMessageDialog(null, "Producto creado con éxito");
			actualizarTabla();
		}

		public void eliminarProductoSeleccionado() {
			Producto producto = obtenerProductoSeleccionado();
			if (producto == null)
				return;

			boolean deleteIsSuccess = almacenamiento.eliminarProducto(producto.getCodigo());
			if (!deleteIsSuccess) {
				JOptionPane.showMessageDialog(null, "Hubo un error al eliminar el producto");
				return;
			}

			JOptionPane.showMessageDialog(null, "Producto eliminado con éxito");
			actualizarTabla();
		}

		private void actualizarTabla() {
			List<Producto> producto = almacenamiento.listarProducto();
			rellenarTabla(producto);
		}

		public void buscarProducto() {
			String codigo = txtCodigoProducto.getText();
			List<Producto> producto = almacenamiento.buscarProductoPorCodigo(codigo);
			rellenarTabla(producto);
		}

		public void actualizarProducto(Producto productoActualizado) {
			boolean updateIsSuccess = almacenamiento.actualizarProducto(productoActualizado);
			if (!updateIsSuccess) {
				JOptionPane.showMessageDialog(null, "Hubo un error al actualizar el producto");
				return;
			}

			JOptionPane.showMessageDialog(null, "Producto actualizado con éxito");
			actualizarTabla();
		}
	}

}
