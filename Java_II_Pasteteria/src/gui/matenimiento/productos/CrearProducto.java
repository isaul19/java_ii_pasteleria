package gui.matenimiento.productos;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.matenimiento.productos.MantenimientoProductos.ProductoHandler;
import models.Cliente;
import models.Producto;
import utils.Validate;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;

public class CrearProducto extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtStockActual;
	private JTextField txtStockMinimo;
	private JTextField txtStockMaximo;

	private JButton btnCrear;
	private JButton btnCancelar;
	private ProductoHandler productoHandler;

	public CrearProducto(ProductoHandler productoHandler) {

		this.productoHandler = productoHandler;
		cargarInterfaz();

	}

	private void cargarInterfaz() {
		setBounds(100, 100, 452, 367);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(62, 114, 122, 23);
		contentPanel.add(lblNombre);

		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(62, 154, 122, 23);
		contentPanel.add(lblPrecio);

		txtNombre = new JTextField();
		txtNombre.setBounds(188, 114, 211, 23);
		contentPanel.add(txtNombre);
		txtNombre.setColumns(10);

		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(188, 154, 211, 23);
		contentPanel.add(txtPrecio);

		btnCrear = new JButton("Crear");
		btnCrear.setBounds(79, 248, 89, 23);
		btnCrear.addActionListener(this);
		contentPanel.add(btnCrear);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(220, 248, 89, 23);
		btnCancelar.addActionListener(this);
		contentPanel.add(btnCancelar);

		JLabel lblTitle = new JLabel("Crear Producto");
		lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 21));
		lblTitle.setBounds(141, 27, 156, 31);
		contentPanel.add(lblTitle);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnCrear) {
			guardarProducto();
		}

		if (e.getSource() == btnCancelar) {
			dispose();
		}
	}

	private void guardarProducto() {
		if (!formularioRellenadoCorrectamente())
			return;

		String Nombre = txtNombre.getText().trim();
		double Precio = Double.parseDouble(txtPrecio.getText().trim());
		int Stock_Actual = 0;
		int Stock_Minimo = 0;
		int Stock_Maximo = 0;

		Producto nuevoProducto = new Producto(Nombre, Precio, Stock_Actual, Stock_Minimo, Stock_Maximo);
		productoHandler.crearProducto(nuevoProducto);
		dispose();
	}

	private boolean formularioRellenadoCorrectamente() {
		if (Validate.isEmpty(txtNombre.getText(), "Nombre")) {
			return false;
		}

		if (Validate.isPositiveNumeric(txtPrecio.getText(), "Precio")) {
			return false;
		}

		return true;
	}
}
