package gui.matenimiento.clientes;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import gui.matenimiento.clientes.MantenimientoClientesGui.ClienteHandler;
import models.Cliente;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VerClienteGui extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private JTextField txtCodigo;
	private JTextField txtDni;
	private JTextField txtNombres;
	private JTextField txtApellidos;
	private JTextField txtTelefono;
	private JTextField txtDireccion;

	private JButton btnCerrar;

	public VerClienteGui(ClienteHandler clienteHandler) {
		cargarInterfaz();
		Cliente cliente = clienteHandler.obtenerClienteSeleccionado();
		mostrarCliente(cliente);

	}

	private void cargarInterfaz() {
		setTitle("Ver Cliente");
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		setLocationRelativeTo(null);

		JLabel lblCodigo = new JLabel("Código:");
		lblCodigo.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblCodigo.setBounds(50, 30, 70, 20);
		contentPanel.add(lblCodigo);

		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(130, 30, 200, 20);
		contentPanel.add(txtCodigo);
		txtCodigo.setColumns(10);

		JLabel lblDni = new JLabel("DNI:");
		lblDni.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblDni.setBounds(50, 60, 70, 20);
		contentPanel.add(lblDni);

		txtDni = new JTextField();
		txtDni.setEditable(false);
		txtDni.setBounds(130, 60, 200, 20);
		contentPanel.add(txtDni);
		txtDni.setColumns(10);

		JLabel lblNombres = new JLabel("Nombres:");
		lblNombres.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblNombres.setBounds(50, 90, 70, 20);
		contentPanel.add(lblNombres);

		txtNombres = new JTextField();
		txtNombres.setEditable(false);
		txtNombres.setBounds(130, 90, 200, 20);
		contentPanel.add(txtNombres);
		txtNombres.setColumns(10);

		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblApellidos.setBounds(50, 120, 70, 20);
		contentPanel.add(lblApellidos);

		txtApellidos = new JTextField();
		txtApellidos.setEditable(false);
		txtApellidos.setBounds(130, 120, 200, 20);
		contentPanel.add(txtApellidos);
		txtApellidos.setColumns(10);

		JLabel lblTelefono = new JLabel("Teléfono:");
		lblTelefono.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblTelefono.setBounds(50, 150, 70, 20);
		contentPanel.add(lblTelefono);

		txtTelefono = new JTextField();
		txtTelefono.setEditable(false);
		txtTelefono.setBounds(130, 150, 200, 20);
		contentPanel.add(txtTelefono);
		txtTelefono.setColumns(10);

		JLabel lblDireccion = new JLabel("Dirección:");
		lblDireccion.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblDireccion.setBounds(50, 180, 70, 20);
		contentPanel.add(lblDireccion);

		txtDireccion = new JTextField();
		txtDireccion.setEditable(false);
		txtDireccion.setBounds(130, 180, 200, 20);
		contentPanel.add(txtDireccion);
		txtDireccion.setColumns(10);

		btnCerrar = new JButton("Cerrar");
		btnCerrar.setFont(new Font("Segoe UI", Font.BOLD, 12));
		btnCerrar.setBounds(180, 220, 100, 25);
		btnCerrar.addActionListener(this);
		contentPanel.add(btnCerrar);

	}

	private void mostrarCliente(Cliente cliente) {
		if (cliente != null) {
			txtCodigo.setText(cliente.getCodigo().toString());
			txtDni.setText(String.valueOf(cliente.getDni()));
			txtNombres.setText(cliente.getNombres());
			txtApellidos.setText(cliente.getApellidos());
			txtTelefono.setText(String.valueOf(cliente.getTelefono()));
			txtDireccion.setText(cliente.getDireccion());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnCerrar) {
			dispose();
		}
	}
}
