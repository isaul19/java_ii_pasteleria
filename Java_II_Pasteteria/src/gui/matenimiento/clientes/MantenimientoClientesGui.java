package gui.matenimiento.clientes;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import models.Cliente;
import storage.AlmacenamientoClientes;
import utils.CustomImage;

import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

public class MantenimientoClientesGui extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private AlmacenamientoClientes almacenamiento = new AlmacenamientoClientes();
	ClienteHandler clienteHandler = new ClienteHandler();

	private JTable table;
	private JTextField txtCodigoCliente;
	JButton btnVer;
	JButton btnEditar;
	JButton btnEliminar;
	JButton btnCrear;

	public MantenimientoClientesGui() {
		cargarInterfaz();
		inicializarDatosDeLaTabla();
	}

	private void cargarInterfaz() {
		setTitle("Gestión de Clientes");
		setBounds(100, 100, 700, 746);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		table = new JTable();
		table.setBounds(27, 374, 633, 257);
		DefaultTableModel model = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		model.addColumn("Código de Cliente");
		model.addColumn("DNI");
		model.addColumn("Nombres");
		model.addColumn("Apellidos");
		table.setModel(model);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(27, 374, 633, 257);
		contentPanel.add(scrollPane);

		btnVer = new JButton("Ver");
		btnVer.setFont(new Font("Segoe UI", Font.BOLD, 11));
		btnVer.setBounds(30, 660, 120, 23);
		ImageIcon showIcon = CustomImage.create("show.png");
		btnVer.setIcon(showIcon);
		btnVer.addActionListener(this);
		contentPanel.add(btnVer);

		btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Segoe UI", Font.BOLD, 11));
		btnEditar.setBounds(277, 660, 120, 23);
		ImageIcon editIcon = CustomImage.create("edit.png");
		btnEditar.setIcon(editIcon);
		btnEditar.addActionListener(this);
		contentPanel.add(btnEditar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setFont(new Font("Segoe UI", Font.BOLD, 11));
		ImageIcon deleteIcon = CustomImage.create("delete.png");
		btnEliminar.setBounds(539, 660, 120, 23);
		btnEliminar.setIcon(deleteIcon);
		btnEliminar.addActionListener(this);
		contentPanel.add(btnEliminar);

		JLabel lblTitulo = new JLabel("Gestión de Clientes");
		lblTitulo.setFont(new Font("Segoe UI", Font.BOLD, 24));
		lblTitulo.setBounds(30, 10, 239, 43);
		contentPanel.add(lblTitulo);

		JLabel lblSubtitulo1 = new JLabel("Crear Cliente");
		lblSubtitulo1.setFont(new Font("Segoe UI", Font.BOLD, 20));
		lblSubtitulo1.setBounds(30, 70, 197, 34);
		contentPanel.add(lblSubtitulo1);

		JLabel lblDescripcion1 = new JLabel(
				"<html>Haz clic en este botón para abrir una pestaña <br>donde podrás ingresar la información de un nuevo cliente.</html>");
		lblDescripcion1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblDescripcion1.setBounds(30, 110, 367, 50);
		contentPanel.add(lblDescripcion1);

		btnCrear = new JButton("");
		btnCrear.setBounds(30, 180, 89, 23);
		ImageIcon addClientIcon = CustomImage.create("add-client.png");
		btnCrear.setIcon(addClientIcon);
		btnCrear.addActionListener(this);
		contentPanel.add(btnCrear);

		JLabel lblSubtitulo2 = new JLabel("Buscar Cliente");
		lblSubtitulo2.setFont(new Font("Segoe UI", Font.BOLD, 20));
		lblSubtitulo2.setBounds(30, 220, 197, 34);
		contentPanel.add(lblSubtitulo2);

		JLabel lblDescripcion2 = new JLabel(
				"<html>Ingresa el código del cliente en el campo de búsqueda <br>para encontrar los detalles del cliente.</html>");
		lblDescripcion2.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblDescripcion2.setBounds(30, 260, 367, 55);
		contentPanel.add(lblDescripcion2);

		txtCodigoCliente = new JTextField();
		txtCodigoCliente.setBounds(30, 330, 239, 25);
		escucharCambiosDeCodigoCliente();
		contentPanel.add(txtCodigoCliente);
		txtCodigoCliente.setColumns(10);
	}

	private void escucharCambiosDeCodigoCliente() {
		txtCodigoCliente.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				clienteHandler.buscarCliente();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				clienteHandler.buscarCliente();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				clienteHandler.buscarCliente();
			}
		});

	}

	private void rellenarTabla(List<Cliente> products) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);

		for (int i = 0; i < products.size(); i++) {
			Cliente cliente = products.get(i);
			Object[] row = { cliente.getCodigo(), cliente.getDni(), cliente.getNombres(), cliente.getApellidos() };
			model.addRow(row);
		}
	}

	private void inicializarDatosDeLaTabla() {
		List<Cliente> clientes = almacenamiento.listarClientes();
		rellenarTabla(clientes);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnCrear) {
			new CrearClienteGui(clienteHandler).setVisible(true);
			return;
		}

		if (clienteHandler.obtenerClienteSeleccionado() == null) {
			return;
		}

		if (e.getSource() == btnVer) {
			new VerClienteGui(clienteHandler).setVisible(true);
		}

		if (e.getSource() == btnEditar) {
			new ActualizarClienteGui(clienteHandler).setVisible(true);

		}

		if (e.getSource() == btnEliminar) {
			clienteHandler.eliminarClienteSeleccionado();
		}

	}

	public class ClienteHandler {
		public Cliente obtenerClienteSeleccionado() {
			int selectedRow = table.getSelectedRow();
			if (selectedRow != -1) {
				int clienteCodigo = (int) table.getValueAt(selectedRow, 0);
				Cliente cliente = almacenamiento.obtenerClientePorCodigo(clienteCodigo);
				return cliente;
			}

			JOptionPane.showMessageDialog(null, "Primero selecciona una fila");
			return null;
		}

		public void crearCliente(Cliente cliente) {
			int clienteCodigo = almacenamiento.guardar(cliente);
			if (clienteCodigo == -1) {
				JOptionPane.showMessageDialog(null, "Hubo un error al crear el cliente");
				return;
			}

			JOptionPane.showMessageDialog(null, "Cliente creado con éxito");
			actualizarTabla();
		}

		public void eliminarClienteSeleccionado() {
			Cliente cliente = obtenerClienteSeleccionado();
			if (cliente == null)
				return;

			boolean deleteIsSuccess = almacenamiento.eliminarCliente(cliente.getCodigo());
			if (!deleteIsSuccess) {
				JOptionPane.showMessageDialog(null, "Hubo un error al eliminar el cliente");
				return;
			}

			JOptionPane.showMessageDialog(null, "Cliente eliminado con éxito");
			actualizarTabla();
		}

		private void actualizarTabla() {
			List<Cliente> clientes = almacenamiento.listarClientes();
			rellenarTabla(clientes);
		}

		public void buscarCliente() {
			String codigo = txtCodigoCliente.getText();
			List<Cliente> clientes = almacenamiento.buscarClientePorCodigo(codigo);
			rellenarTabla(clientes);
		}

		public void actualizarCliente(Cliente clienteActualizado) {
			boolean updateIsSuccess = almacenamiento.actualizarCliente(clienteActualizado);
			if (!updateIsSuccess) {
				JOptionPane.showMessageDialog(null, "Hubo un error al actualizar el cliente");
				return;
			}

			JOptionPane.showMessageDialog(null, "Cliente actualizado con éxito");
			actualizarTabla();
		}
	}
}
