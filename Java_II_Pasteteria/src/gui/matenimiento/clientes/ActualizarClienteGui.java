package gui.matenimiento.clientes;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import gui.matenimiento.clientes.MantenimientoClientesGui.ClienteHandler;
import models.Cliente;
import utils.Validate;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActualizarClienteGui extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private Cliente cliente;
	private ClienteHandler clienteHandler;

	private JTextField txtCodigo;
	private JTextField txtDni;
	private JTextField txtNombres;
	private JTextField txtApellidos;
	private JTextField txtTelefono;
	private JTextField txtDireccion;

	private JButton btnSave;
	private JButton btnCancel;

	public ActualizarClienteGui(ClienteHandler clienteHandler) {
		this.clienteHandler = clienteHandler;
		this.cliente = clienteHandler.obtenerClienteSeleccionado();

		cargarInterfaz();
		cargarDatosCliente();
	}

	private void cargarInterfaz() {
		setTitle("Actualizar Cliente");
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		setLocationRelativeTo(null);

		JLabel lblCodigo = new JLabel("Código:");
		lblCodigo.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblCodigo.setBounds(50, 30, 70, 20);
		contentPanel.add(lblCodigo);

		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(130, 30, 200, 20);
		contentPanel.add(txtCodigo);
		txtCodigo.setColumns(10);

		JLabel lblDni = new JLabel("DNI:");
		lblDni.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblDni.setBounds(50, 60, 70, 20);
		contentPanel.add(lblDni);

		txtDni = new JTextField();
		txtDni.setBounds(130, 60, 200, 20);
		contentPanel.add(txtDni);
		txtDni.setColumns(10);

		JLabel lblNombres = new JLabel("Nombres:");
		lblNombres.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblNombres.setBounds(50, 90, 70, 20);
		contentPanel.add(lblNombres);

		txtNombres = new JTextField();
		txtNombres.setBounds(130, 90, 200, 20);
		contentPanel.add(txtNombres);
		txtNombres.setColumns(10);

		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblApellidos.setBounds(50, 120, 70, 20);
		contentPanel.add(lblApellidos);

		txtApellidos = new JTextField();
		txtApellidos.setBounds(130, 120, 200, 20);
		contentPanel.add(txtApellidos);
		txtApellidos.setColumns(10);

		JLabel lblTelefono = new JLabel("Teléfono:");
		lblTelefono.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblTelefono.setBounds(50, 150, 70, 20);
		contentPanel.add(lblTelefono);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(130, 150, 200, 20);
		contentPanel.add(txtTelefono);
		txtTelefono.setColumns(10);

		JLabel lblDireccion = new JLabel("Dirección:");
		lblDireccion.setFont(new Font("Segoe UI", Font.BOLD, 14));
		lblDireccion.setBounds(50, 180, 70, 20);
		contentPanel.add(lblDireccion);

		txtDireccion = new JTextField();
		txtDireccion.setBounds(130, 180, 200, 20);
		contentPanel.add(txtDireccion);
		txtDireccion.setColumns(10);

		btnSave = new JButton("Guardar");
		btnSave.setFont(new Font("Segoe UI", Font.BOLD, 12));
		btnSave.setBounds(100, 220, 100, 25);
		btnSave.addActionListener(this);
		contentPanel.add(btnSave);

		btnCancel = new JButton("Cancelar");
		btnCancel.setFont(new Font("Segoe UI", Font.BOLD, 12));
		btnCancel.setBounds(220, 220, 100, 25);
		btnCancel.addActionListener(this);
		contentPanel.add(btnCancel);
	}

	private void cargarDatosCliente() {
		if (cliente != null) {
			txtCodigo.setText(cliente.getCodigo().toString());
			txtDni.setText(String.valueOf(cliente.getDni()));
			txtNombres.setText(cliente.getNombres());
			txtApellidos.setText(cliente.getApellidos());
			txtTelefono.setText(String.valueOf(cliente.getTelefono()));
			txtDireccion.setText(cliente.getDireccion());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnSave) {
			actualizarCliente();
		}

		if (e.getSource() == btnCancel) {
			dispose();
		}
	}

	private void actualizarCliente() {
		if (!formularioRellenadoCorrectamente()) {
			return;
		}

		int nuevoDni = Integer.parseInt(txtDni.getText().trim());
		String nuevoNombres = txtNombres.getText().trim();
		String nuevoApellidos = txtApellidos.getText().trim();
		int nuevoTelefono = Integer.parseInt(txtTelefono.getText().trim());
		String nuevoDireccion = txtDireccion.getText().trim();

		cliente.setDni(nuevoDni);
		cliente.setNombres(nuevoNombres);
		cliente.setApellidos(nuevoApellidos);
		cliente.setTelefono(nuevoTelefono);
		cliente.setDireccion(nuevoDireccion);

		clienteHandler.actualizarCliente(cliente);
		dispose();
	}

	private boolean formularioRellenadoCorrectamente() {

		if (!Validate.hasExactLength(txtDni.getText(), 8, "DNI")) {
			return false;
		}
		if (Validate.isEmpty(txtNombres.getText(), "Nombres")) {
			return false;
		}

		if (Validate.isEmpty(txtApellidos.getText(), "Apellidos")) {
			return false;
		}

		if (!Validate.hasMinLength(txtTelefono.getText(), 9, "Teléfono")) {
			return false;
		}

		if (Validate.isEmpty(txtDireccion.getText(), "Dirección")) {
			return false;
		}

		return true;
	}
}
