package gui.almacen;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;

import models.Producto;
import storage.AlmacenamientoProductos;

public class MantenimientoStockGui extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private AlmacenamientoProductos almacenamiento = new AlmacenamientoProductos();
	ProductoStockHandler productoStockHandler = new ProductoStockHandler();

	private JTable table;
	private JTextField txtCodigoProducto;
	private JButton btnAgregarStock;

	public MantenimientoStockGui() {
		setBounds(100, 100, 577, 599);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 220, 541, 240);
		contentPanel.add(scrollPane);

		table = new JTable();
		DefaultTableModel model = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		model.addColumn("Código de producto");
		model.addColumn("Nombre");
		model.addColumn("Stock Actual");
		model.addColumn("Stock Minimo");
		model.addColumn("Stock Maximo");
		table.setModel(model);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);

		inicializarDatosDeLaTabla();

		JLabel lblTitulo = new JLabel("Ingresar Stock");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblTitulo.setBounds(175, 11, 292, 38);
		contentPanel.add(lblTitulo);

		JLabel lblSubtitulo2 = new JLabel("Buscar Producto");
		lblSubtitulo2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSubtitulo2.setBounds(10, 90, 178, 30);
		contentPanel.add(lblSubtitulo2);

		JLabel lblDescripcion2 = new JLabel(
				"<html>Ingresa el codigo del producto en el campo de búsqueda <br> para encontar detalles del producto <html>");
		lblDescripcion2.setBounds(10, 131, 361, 46);
		contentPanel.add(lblDescripcion2);

		txtCodigoProducto = new JTextField();
		txtCodigoProducto.setBounds(10, 188, 212, 20);
		contentPanel.add(txtCodigoProducto);
		txtCodigoProducto.setColumns(10);

		escucharCambiosDeCodigoProducto();

		btnAgregarStock = new JButton("Modificar Stock");
		btnAgregarStock.setBounds(207, 512, 137, 23);
		btnAgregarStock.addActionListener(this);
		contentPanel.add(btnAgregarStock);

	}

	private void escucharCambiosDeCodigoProducto() {
		txtCodigoProducto.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				productoStockHandler.buscarProducto();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				productoStockHandler.buscarProducto();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				productoStockHandler.buscarProducto();
			}
		});

	}

	private void rellenarTabla(List<Producto> products) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);

		for (int i = 0; i < products.size(); i++) {
			Producto prodcuto = products.get(i);
			Object[] row = { prodcuto.getCodigo(), prodcuto.getNombre(), prodcuto.getStock(), prodcuto.getStockMinimo(),
					prodcuto.getStockMaximo() };
			model.addRow(row);
		}
	}

	private void inicializarDatosDeLaTabla() {
		List<Producto> productos = almacenamiento.listarProducto();
		rellenarTabla(productos);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (productoStockHandler.obtenerProductoSeleccionado() == null) {
			return;
		}

		if (e.getSource() == btnAgregarStock) {
			new AgregarStock(productoStockHandler).setVisible(true);
		}
	}

	public class ProductoStockHandler {
		public Producto obtenerProductoSeleccionado() {
			int selectedRow = table.getSelectedRow();
			if (selectedRow != -1) {
				int productoCodigo = (int) table.getValueAt(selectedRow, 0);
				Producto producto = almacenamiento.obtenerProductoPorCodigo(productoCodigo);
				return producto;
			}

			JOptionPane.showMessageDialog(null, "Primero selecciona una fila");
			return null;
		}

		public void crearProducto(Producto producto) {
			int productoCodigo = almacenamiento.guardar(producto);
			if (productoCodigo == -1) {
				JOptionPane.showMessageDialog(null, "Hubo un error al crear el producto");
				return;
			}

			JOptionPane.showMessageDialog(null, "Producto creado con éxito");
			actualizarTabla();
		}

		public void eliminarProductoSeleccionado() {
			Producto producto = obtenerProductoSeleccionado();
			if (producto == null)
				return;

			boolean deleteIsSuccess = almacenamiento.eliminarProducto(producto.getCodigo());
			if (!deleteIsSuccess) {
				JOptionPane.showMessageDialog(null, "Hubo un error al eliminar el producto");
				return;
			}

			JOptionPane.showMessageDialog(null, "Producto eliminado con éxito");
			actualizarTabla();
		}

		private void actualizarTabla() {
			List<Producto> producto = almacenamiento.listarProducto();
			rellenarTabla(producto);
		}

		public void buscarProducto() {
			String codigo = txtCodigoProducto.getText();
			List<Producto> producto = almacenamiento.buscarProductoPorCodigo(codigo);
			rellenarTabla(producto);
		}

		public void actualizarProducto(Producto productoActualizado) {
			boolean updateIsSuccess = almacenamiento.actualizarProducto(productoActualizado);
			if (!updateIsSuccess) {
				JOptionPane.showMessageDialog(null, "Hubo un error al actualizar el producto");
				return;
			}

			JOptionPane.showMessageDialog(null, "Producto actualizado con éxito");
			actualizarTabla();
		}
	}

}
