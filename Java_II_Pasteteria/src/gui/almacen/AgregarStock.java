package gui.almacen;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.*;
import java.awt.event.ActionEvent;

import gui.almacen.MantenimientoStockGui.ProductoStockHandler;
import models.Producto;
import utils.Validate;

public class AgregarStock extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	private Producto producto;
	private ProductoStockHandler productoStockHandler;

	private JTextField txtStockActual;
	private JTextField txtStockMinimo;
	private JTextField txtStockMaximo;

	private JButton btnActualizar;
	private JButton btnCancelar;

	public AgregarStock(ProductoStockHandler productoStockHandler) {

		this.productoStockHandler = productoStockHandler;
		this.producto = productoStockHandler.obtenerProductoSeleccionado();

		cargarInterfaz();
		cargarDatosProducto();
	}

	private void cargarInterfaz() {
		setTitle("Actualizar Producto");
		setBounds(100, 100, 437, 399);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblStockActual = new JLabel("Stock Actual");
		lblStockActual.setBounds(82, 124, 101, 25);
		contentPanel.add(lblStockActual);

		JLabel lblStockMinimo = new JLabel("Stock Minimo");
		lblStockMinimo.setBounds(82, 169, 89, 25);
		contentPanel.add(lblStockMinimo);

		JLabel lblStockMaximo = new JLabel("Stock Maximo");
		lblStockMaximo.setBounds(82, 213, 89, 25);
		contentPanel.add(lblStockMaximo);

		txtStockActual = new JTextField();
		txtStockActual.setBounds(175, 125, 217, 23);
		contentPanel.add(txtStockActual);

		txtStockMinimo = new JTextField();
		txtStockMinimo.setColumns(10);
		txtStockMinimo.setBounds(175, 170, 217, 23);
		contentPanel.add(txtStockMinimo);

		txtStockMaximo = new JTextField();
		txtStockMaximo.setColumns(10);
		txtStockMaximo.setBounds(175, 214, 217, 23);
		contentPanel.add(txtStockMaximo);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(82, 278, 89, 23);
		btnCancelar.addActionListener(this);
		contentPanel.add(btnCancelar);

		btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(this);
		btnActualizar.setBounds(243, 278, 89, 23);
		contentPanel.add(btnActualizar);

		JLabel lblTitle = new JLabel("Agregar Stock");
		lblTitle.setFont(new Font("Segoe UI", Font.BOLD, 21));
		lblTitle.setBounds(121, 23, 205, 38);
		contentPanel.add(lblTitle);
	}

	private void cargarDatosProducto() {
		if (producto != null) {
			txtStockActual.setText(String.valueOf(producto.getStock()));
			txtStockMinimo.setText(String.valueOf(producto.getStockMinimo()));
			txtStockMaximo.setText(String.valueOf(producto.getStockMaximo()));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnActualizar) {
			actualizarProducto();
		}

		if (e.getSource() == btnCancelar) {
			dispose();
		}
	}

	private void actualizarProducto() {
		if (!formularioLlenadoCorrectamente()) {
			return;
		}

		int stockActual = Integer.parseInt(txtStockActual.getText());
		int stockMinimo = Integer.parseInt(txtStockMinimo.getText());
		int stockMaximo = Integer.parseInt(txtStockMaximo.getText());

		producto.setStock_Actual(stockActual);
		producto.setStock_Minimo(stockMinimo);
		producto.setStock_Maximo(stockMaximo);

		productoStockHandler.actualizarProducto(producto);
		dispose();
	}

	private boolean formularioLlenadoCorrectamente() {
		if (!Validate.isPositiveInteger(txtStockActual.getText(), "Stock Actual")) {
			return false;
		}

		if (!Validate.isPositiveInteger(txtStockMinimo.getText(), "Stock Minimo")) {
			return false;
		}

		if (!Validate.isPositiveInteger(txtStockMaximo.getText(), "Stock Maximo")) {
			return false;
		}

		double stockMaximo = Double.parseDouble(txtStockMaximo.getText());
		double stockMinimo = Double.parseDouble(txtStockMinimo.getText());

		if (!Validate.isLessThan(txtStockMinimo.getText(), stockMaximo, "Stock Minimo")) {
			return false;
		}

		if (!Validate.isLessThanOrEqual(txtStockActual.getText(), stockMaximo, "Stock Actual")) {
			return false;
		}

		if (!Validate.isGreaterThan(txtStockActual.getText(), stockMinimo, "Stock Actual")) {
			return false;
		}

		return true;
	}
}
